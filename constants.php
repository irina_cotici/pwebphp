<?php
  define('USER_ROLES', [
    0 => 'Admin',
    1 => 'Collaborator'
  ]);

  define('USER_STATUSES', [
    0 => 'Active',
    1 => 'Inactive'
  ]);

  define('PROFILE_PLACEHOLDER', dirname($_SERVER['PHP_SELF']) . "/assets/profile/placeholder.png")
?>
