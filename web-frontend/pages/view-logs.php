<?php
checkRedirect();

$connection = getConnection();
$result =  processGetAllLogs($connection);
$logs = [];

if ($result['success']) {
	$logs = $result['data'];
}

function renderLogs($logs) {
	$html = <<<HTML
		<div class="container section-60">
	HTML;

	foreach($logs as $log) {
		$author = $log['firstname'] && $log['lastname'] ? "{$log['firstname']} {$log['lastname']}" : "Anonymous"; 
		$html .= <<<HTML
			<div class="list-container">
				<div class="user-info">
					<div>
						<strong>Author: </strong>$author
					</div>
					<div>
						<strong>Date: </strong>{$log['timestamp']}
					</div>
					<div>
						<strong>Action: </strong>{$log['message']}
					</div>
				</div>
			</div>
		HTML;
	}

	$html .= <<<HTML
		</div>
	HTML;

	echo $html;
}

renderLogs($logs);
?> 