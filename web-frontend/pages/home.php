	<section class="main-section">
			<div class="section-60">
					<h2>Welcome to Our Site!</h2>
					<p>
						Welcome to our platform! Our site provides a unique space where
						individuals can share their personal experiences and contribute to 
						the betterment of the community. We believe that every incident, 
						no matter how big or small, carries valuable lessons that can lead 
						to positive change.
					</p>
					<p>
						On our platform, you have the opportunity to write about incidents
						that have occurred in your life. Whether it's a heartwarming act of
						kindness, an inspiring success story, a thought-provoking reflection,
						or even a challenging obstacle you overcame, your experiences matter.
						By sharing your stories, you play an active role in creating 
						awareness, empathy, and understanding among our community members.
					</p>
			</div>
			
			<?php
				include 'web-frontend/login.php';
			?>
			
	</section>