<?php
checkRedirect();
$saveAttemptImport = false;
$successImport = false;
$saveAttemptExport = false;
$successExport = false;

function renderFormExport($success, $saveAttempt){
	$html = <<<HTML
		<div class="section-40">
			<form action="" method="POST" enctype="multipart/form-data">
				<input type="text" name="action" hidden value="export">
				<input class="btn-submit" type="submit" name="submit" value="Export Database">
			</form>
	HTML;

	if ($saveAttempt && $success) {
		$html .= <<<HTML
			<div class="success">Database exported sucesfully</div>
		HTML;
	} else if ($saveAttempt && !$success) {
		$html .= <<<HTML
			<div class="invalid-input">An error occured while exporting the database</div>
		HTML;
	}

	echo $html;
}

function renderFormImport($success, $saveAttempt){

	$html = <<<HTML
		<form action="" method="POST" enctype="multipart/form-data">
			<input type="text" name="action" hidden value="import">
			<input class="btn-submit" type="submit" name="submit" value="Import Backup">
		</form>
	HTML;

	if ($saveAttempt && $success) {
		$html .= <<<HTML
			<div class="success">Backup imported sucesfully</div>
		HTML;
	} else if ($saveAttempt && !$success) {
		$html .= <<<HTML
			<div class="invalid-input">An error occured while importing the backup</div>
		HTML;
	}

	$html .= <<<HTML
			</div>
	HTML;

	echo $html;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$action = processInput("action");
	
	if ($action === "import") {
		$saveAttemptImport = true;
		$successImport = importDumpDB("/backup/");
	}
	else if ($action === "export") {
		$saveAttemptExport = true;
		$successExport = exportDumpDB("/backup/");
	}
}

renderFormExport($successExport, $saveAttemptExport);
renderFormImport($successImport, $saveAttemptImport);
?>