<?php
checkRedirect();

require 'web-backend/incident.php';

$connection = getConnection();
$incident = null;
$validationErrors = [];
$incidentId = "";
$success = false;
$spaceKeywords = "";
$confirmed = false;
$type = "";

function renderForm($validationErrors, $confirmed, $incident, $spaceKeywords, $success) {
    $renderTextInput = 'renderTextInput';
    $renderTextArea = 'renderTextArea';
    $renderFlagInput = 'renderFlagInput';
    $renderFileInput = 'renderFileInput';

    $title = isset($incident["title"]) ? $incident["title"] : "";
    $description = isset($incident["description"]) ? $incident["description"] : "";
    $address = isset($incident["address"]) ? $incident["address"] : "";
    $keywords = isset($spaceKeywords) ? $spaceKeywords : "";

    $html = <<<HTML
        <div class="container section-60">
            <form action="" method="POST" enctype="multipart/form-data">
                <h2 class="page-title">Create a new incident</h2>
                {$renderTextInput("Title", "title", $title, "text", $validationErrors)}
                {$renderTextArea("Description", "description", $description, "6", $validationErrors)}
                {$renderTextInput("Address", "address", $address, "text", $validationErrors)}
                {$renderTextInput("Key Words", "keywords", $keywords, "text", $validationErrors)}
    HTML;

    if ($confirmed) {
        $html .= <<<HTML
            {$renderFlagInput("type", "submit")}
            <input class="btn-submit" type="submit" name="submit" value="Confirm Insertion">
            HTML;
        }
    else {
        $html .= <<<HTML
            <input type="text" name="id" hidden value="ceva">
            {$renderFlagInput("type", "confirm")}
            <input class="btn-submit" type="submit" name="submit" value="Add Incident">
        HTML;
    }

    $html .= <<<HTML
            </form>
        </div>
    HTML;

    if ($success) {
        $html .= <<<HTML
            <div class="success">Incident added sucesfully</div>
        HTML;
    }

    echo $html;
}

if (isset($_GET["id"]) && isset($_SESSION['user'])) {
    $incidentId = $_GET["id"];
    $result = processGetIncidentById($connection, $incidentId);
    $incident = $result["data"];
    $spaceKeywords = str_replace(',', ' ', $incident["keywords"]);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $title = processInput("title");
    $type = processInput("type");
    $id = processInput("id");

    if ($type == "confirm") {
        $validationErrors = [];

        $title = processInput("title");
        $description = processInput("description");
        $address = processInput("address");
        $keywords = processInput("keywords");
    
        $validationErrors = validateInput("title", $title, ["required" => true], $validationErrors);
        $validationErrors = validateInput("description", $description, ["required" => true], $validationErrors);
        $validationErrors = validateInput("address", $address, ["required" => true], $validationErrors);
        $validationErrors = validateInput("keywords", $keywords, ["required" => true], $validationErrors);
    
        $incident["title"] = $title;
        $incident["description"] = $description;
        $incident["address"] = $address;
        $spaceKeywords = $keywords;

        if (count($validationErrors) == 0) {
            $confirmed = true;
            $success = false;
        }
    }
    elseif ($type == "submit") {
        $result = processIncidentCreation($connection, $_SESSION['user']['id']);
        $incident = $result["data"];
        $success = $result["success"];
        $confirmed = false;
    }
}

renderForm($validationErrors, $confirmed, $incident, $spaceKeywords, $success);
?>