<?php
require 'web-backend/incident.php';
require 'web-backend/comment.php';
require 'web-backend/vote.php';
require 'web-backend/photo.php';

$connection = getConnection();
$incidentId = isset($_GET["id"]) ? $_GET["id"] : '';
$userId = isset($_SESSION['user']) ? $_SESSION['user']['id'] : '';
$result = processGetAllIncidentPhotos($connection, $incidentId);
$photos = $result["data"];
$validationErrors = [];

function getAllComments($connection, $incidentId) {
    $result = processGetAllIncidentComments($connection, $incidentId);

    if ($result["success"]) {
        return $result["data"];
    }

    return null;
}

$comments = getAllComments($connection, $incidentId);

function countIncidentVotes($connection, &$incident) {
    $incidentId = $incident["id"];

    $result = processGetAllIncidentVotes($connection, $incidentId);

    if ($result["success"]) {
        $votes = $result["data"];

        $positiveCount = 0;
        $negativeCount = 0;

        foreach ($votes as &$vote) {
            if ($vote["isPositive"]) {
                $positiveCount++;
            } else {
                $negativeCount++;
            }
        }

        $incident['positiveVotes'] = $positiveCount;
        $incident['negativeVotes'] = $negativeCount;
    }
}

function getIncident($connection, $incidentId) {
    $result = processGetIncidentById($connection, $incidentId);
    $incident = $result["data"];
    countIncidentVotes($connection, $incident);

    return $incident;
}

$incident = getIncident($connection, $incidentId);

function renderIncident($incident) {
    $positiveVotes = $incident['positiveVotes'];
    $negativeVotes = $incident['negativeVotes'];
    $description = $incident['description'];

    $html = <<<HTML
        <div class="container section-60">
            <div><strong>Incident title: </strong>{$incident['title']}</div>
            <div><strong>Positive votes: </strong>$positiveVotes</div>
            <div><strong>Negative votes: </strong>$negativeVotes</div>
            <div><strong>Description: </strong></div>
            <div>$description</div>
        </div>
    HTML;

    echo $html;      
}

function renderComments($comments) {
    $html = <<<HTML
        <div class="container comment-wrapper section-60">
    HTML;

    foreach ($comments as $comment) {
        $author = $comment['firstname'] && $comment['lastname'] ? "{$comment['firstname']} {$comment['lastname']}" : "Anonymous"; 

        $html .= <<<HTML
            <div class="comment-container">
                <div><b>Author:</b> $author</div>
                <div><b>Date:</b> {$comment['timestamp']}</div>
                <br>
                <div><b>Comment:</b></div>
                <div>{$comment['description']}</div>
            </div>
        HTML;
    }

    $html .= <<<HTML
        </div>
    HTML;

    echo $html;
}

function renderPhotos($photos) {

    if (empty($photos)) {
        return;
    }

    $renderFlagInput = 'renderFlagInput';

    $html = <<<HTML
        <div class="container section-60">
            <h2 class="page-title">Incident's photos</h2>
            <div class="incident-image-wrapper">
    HTML;

    $currentDir = dirname($_SERVER['PHP_SELF']);
    foreach ($photos as $key => $incidentPhoto) {
        $photoPath = $currentDir . $incidentPhoto['photo'];

        $html .= <<<HTML
            <div class="incident-image-container">
                <img class="incident-image" src="$photoPath" alt="incident photo" />
            </div>
        HTML;
    }

    $html .= <<<HTML
            </div>
        </div>
    HTML;

    echo $html;
}

function renderCommentForm($validationErrors) {
    $renderTextArea = 'renderTextArea';

    $html = <<<HTML
        <div class="container section-60">
            <form action="" method="POST" enctype="multipart/form-data">
                {$renderTextArea("Comment", "description", "", "6", $validationErrors)}
                <input class="btn-submit" type="submit" name="submit" value="Add Comment">
            </form>
        </div>
    HTML;

    echo $html;        
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $result = processCommentAdding($connection, $incidentId, $userId);
    $validationErrors = $result["validationErrors"];
    $success = $result["success"];

    if ($success) {
        $comments = getAllComments($connection, $incidentId);
    }
}

renderIncident($incident, $validationErrors);
renderPhotos($photos);
renderCommentForm($validationErrors);
renderComments($comments);
?>