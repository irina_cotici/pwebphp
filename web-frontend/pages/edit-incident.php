<?php
require 'web-backend/incident.php';
require 'web-backend/photo.php';

$connection = getConnection();
$incident = null;
$validationErrors = [];
$incidentId = "";
$success = false;
$photoSuccess = false;
$spaceKeywords = "";
$photos = [];
$photo = "";

function renderEditIncidentForm($validationErrors, $incident, $spaceKeywords, $success) {
    $isAdmin = isset($_SESSION['authenticated']) && $_SESSION['user']['role'] == 0;
    $renderTextInput = 'renderTextInput';
    $renderTextArea = 'renderTextArea';
    $renderFlagInput = 'renderFlagInput';
    $renderSelectInput = 'renderSelectInput';

    $title = isset($incident["title"]) ? $incident["title"] : "";
    $description = isset($incident["description"]) ? $incident["description"] : "";
    $address = isset($incident["address"]) ? $incident["address"] : "";
    $status = isset($incident["status"]) ? $incident["status"] : "";
    $keywords = isset($spaceKeywords) ? $spaceKeywords : "";

    $statusOptions = [
        ["label" => "Pending Verification", "value" => 0],
        ["label" => "Proven", "value" => 1],
        ["label" => "Processed", "value" => 2],
        ["label" => "Unsolvable", "value" => 3],
        ["label" => "Resolved", "value" => 4]
    ];

    $html = <<<HTML
        <div class="container section-60">
            <form action="" method="POST">
                <h2 class="page-title">Edit incident</h2>
                {$renderTextInput("Title", "title", $title, "text", $validationErrors, true)}
                {$renderTextArea("Description", "description", $description, "6", $validationErrors, true)}
                {$renderTextInput("Address", "address", $address, "text", $validationErrors, true)}
                {$renderTextInput("Key Words", "keywords", $keywords, "text", $validationErrors, true)}
    HTML;

    if ($isAdmin) {
        $html .= <<<HTML
                {$renderSelectInput("Status", "status", $status, $statusOptions, $validationErrors, true)}
        HTML;
    }
    else {
        foreach ($statusOptions as $option) {
            $label = $option['label'];
            $value = $option['value'];
        
            if ($status == $value) {
                $html .= <<<HTML
                    <div><strong>Status:</strong> $label</div>
                HTML;
            }
        }
    }

    $html .= <<<HTML
                {$renderFlagInput("type", "submit")}
                <input class="btn-submit" type="submit" name="submit" value="Edit Incident">
            </form>
    HTML;

    if ($success) {
        $html .= <<<HTML
            <div class="success">Incident updated sucesfully</div>
        HTML;
    }

    $html .= <<<HTML
        </div>
    HTML;

    echo $html;
}

function renderPhotos($photos) {

    if (empty($photos)) {
        return;
    }

    $renderFlagInput = 'renderFlagInput';

    $html = <<<HTML
        <div class="container section-60">
            <h2 class="page-title">Incident's photos</h2>
            <div class="incident-image-wrapper">
    HTML;

    $currentDir = dirname($_SERVER['PHP_SELF']);

    foreach ($photos as $key => $incidentPhoto) {
        $photoPath = $currentDir . $incidentPhoto['photo'];

        $html .= <<<HTML
            <div class="incident-image-container">
                <img class="incident-image" src="$photoPath" alt="incident photo" />
                <form action="" method="POST">
                    {$renderFlagInput("type", "delete-photo")}
                    {$renderFlagInput("photoId", $incidentPhoto['id'])}
                    {$renderFlagInput("photoPath", $incidentPhoto['photo'])}
                    <input class="btn-submit" type="submit" name="submit" value="Remove Image">
                </form>
            </div>
        HTML;
    }

    $html .= <<<HTML
            </div>
        </div>
    HTML;

    echo $html;
}

function renderAddPhotoForm($incidentId, $photo, $success) {
    $renderFileInput = 'renderFileInput';
    $renderFlagInput = 'renderFlagInput';

    $html = <<<HTML
        <div class="container section-60">
            <form action="" method="POST" enctype="multipart/form-data">
                <h2 class="page-title">Add a photo to the incident</h2>
                {$renderFlagInput("type", "upload-photo")}
                {$renderFileInput("Photo", "photo", "image/png, image/gif, image/jpeg")}
                <input class="btn-submit" type="submit" name="submit" value="Add Photo">
            </form>
    HTML;

    if ($success) {
        $html .= <<<HTML
            <div class="success">Photo Added Sucesfully</div>
        HTML;
    }

    $html .= <<<HTML
        </div>
    HTML;

    echo $html;
}

if (isset($_GET["id"]) && isset($_SESSION['user'])) {
    $incidentId = $_GET["id"];
    $result = processGetIncidentById($connection, $incidentId);
    $incident = $result["data"];
    $result = processGetAllIncidentPhotos($connection, $incident["id"]);
    $photos = $result["data"];
    $spaceKeywords = str_replace(',', ' ', $incident["keywords"]);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $title = processInput("title");
    $type = processInput("type");
    $id = processInput("id");

    if ($type == "submit") {
        $result = processIncidentUpdate($connection, $incidentId);
        $incident = $result["data"];
        $spaceKeywords = str_replace(',', ' ', isset($incident["keywords"]) ? $incident["keywords"] : "");
        $success = $result["success"];
        $validationErrors = $result["validationErrors"];
    }
    elseif ($type == "upload-photo") {
        $result = processPhotoAdding($connection, $incidentId);
        $photo = $result["data"];
        $photoSuccess = $result["success"];

        $result = processGetAllIncidentPhotos($connection, $incident["id"]);
        $photos = $result["data"];
    }
    elseif ($type == "delete-photo") {
        $photoId = processInput("photoId");
        $photoPath = processInput("photoPath");
        
        $result = processIncidentPhotoDelete($connection, $photoId, $photoPath);
        $result = processGetAllIncidentPhotos($connection, $incident["id"]);
        $photos = $result["data"];
    }

    if ($success) {

    }

    $type = "";
}

renderEditIncidentForm($validationErrors, $incident, $spaceKeywords, $success);
renderPhotos($photos);
renderAddPhotoForm($incidentId, $photo, $photoSuccess);
?>