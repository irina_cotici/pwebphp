<?php
require 'web-backend/incident.php';
require 'web-backend/vote.php';

$connection = getConnection();
$isAdmin = isset($_SESSION['authenticated']) && $_SESSION['user']['role'] == 0;
$isAuth = isset($_SESSION['authenticated']) && $_SESSION['authenticated'] === true;
$anonymousToken = isset($_SESSION['anonymousToken']) ? $_SESSION['anonymousToken'] : '';
$userId = isset($_SESSION['user']) ? $_SESSION['user']['id'] : '';

function countIncidentVotes($connection, &$incident) {
    $incidentId = $incident["id"];

    $result = processGetAllIncidentVotes($connection, $incidentId);

    if ($result["success"]) {
        $votes = $result["data"];

        $positiveCount = 0;
        $negativeCount = 0;

        foreach ($votes as &$vote) {
            if ($vote["isPositive"]) {
                $positiveCount++;
            } else {
                $negativeCount++;
            }
        }

        $incident['positiveVotes'] = $positiveCount;
        $incident['negativeVotes'] = $negativeCount;
    }
}

function getIncidents($connection, $userId) {
    $result = processGetUserIncidents($connection, $userId);
    $incidents = $result["data"];

    foreach ($incidents as &$incident) {
        countIncidentVotes($connection, $incident);
    }

    return $incidents;
}

$incidents = getIncidents($connection, $userId);

function renderIncident($incident, $isAuth, $isAdmin) {
    $renderFlagInput = 'renderFlagInput';
    $incidentId = $incident['id'];
    $positiveVotes = $incident['positiveVotes'];
    $negativeVotes = $incident['negativeVotes'];
    $status = $incident['status'];
    $statusOptions = [
        ["label" => "Pending Verification", "value" => 0],
        ["label" => "Proven", "value" => 1],
        ["label" => "Processed", "value" => 2],
        ["label" => "Unsolvable", "value" => 3],
        ["label" => "Resolved", "value" => 4]
    ];

    $html = <<<HTML
        <div class="list-container">
        <div><strong>{$incident['title']}</strong></div>
    HTML;

    foreach ($statusOptions as $option) {
        $label = $option['label'];
        $value = $option['value'];
    
        if ($status == $value) {
            $html .= <<<HTML
                <div><strong>Status:</strong> $label</div>
            HTML;
        }
    }

    $html .= <<<HTML
        <div>Positive $positiveVotes</div>
        <div>Negative $negativeVotes</div>
        <div class="buttons-container">
            <a class="btn-submit btn-secondary" href="index.php?page=view-incident&id=$incidentId">Add Comment</a>
    HTML;

    if ($isAuth) {
        $html .= <<<HTML
            <a class="btn-submit btn-secondary" href="index.php?page=edit-incident&id=$incidentId">Edit</a>
        HTML;
    }

    if ($isAdmin) {
        $html .= <<<HTML
                    <form action="" method="POST">
                        {$renderFlagInput("incidentId", "$incidentId")}
                        {$renderFlagInput("type", "delete")}
                        <input class="btn-submit btn-secondary" type="submit" name="submit" value="Delete">
                    </form>
                </div>
            </div>
        HTML;
    }

    return $html;        
}


function renderIncidents($incidents, $isAuth, $isAdmin) {
    $html = <<<HTML
        <h2 class="page-title">My incidents</h2>
        HTML;
        
        foreach ($incidents as $key => $incident) {
        $html .= renderIncident($incident, $isAuth, $isAdmin);
    }


    $html .= <<<HTML
            </div>
        </div>
    HTML;

    echo $html;
}

if (!$isAuth && !$anonymousToken) {
    $anonymousToken = time() . '_' . bin2hex(random_bytes(10));
    $_SESSION['anonymousToken'] = $anonymousToken;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $type = processInput("type");
    $incidentId = processInput("incidentId");

    if ($type == "vote-positive") {
        processVoteAdding(
            $connection,
            true,
            $anonymousToken,
            $incidentId,
            $userId,
        );

        $incidents = getIncidents($connection);
    }
    elseif ($type == "vote-negative") {
        processVoteAdding(
            $connection,
            false,
            $anonymousToken,
            $incidentId,
            $userId,
        );

        $incidents = getIncidents($connection);
    }
    elseif ($type == "delete") {
        processIncidentDelete($connection, $incidentId);
        $incidents = getIncidents($connection);

    }
}

renderIncidents($incidents, $isAuth, $isAdmin);
?>