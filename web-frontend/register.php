<?php

$connection = getConnection();
$user = null;
$validationErrors = [];

function renderForm($validationErrors) {
    $renderTextInput = 'renderTextInput';
    $renderFileInput = 'renderFileInput';
    $renderSelectInput = 'renderSelectInput';

    $roleOptions = [
        ["label" => "Admin", "value" => 0],
        ["label" => "Moderator", "value" => 1]
    ];

    $statusOptions = [
        ["label" => "Active", "value" => 0],
        ["label" => "Disabled", "value" => 1]
    ];

    $html = <<<HTML
        <a class="nav-button" href="index.php?page=manage-users">List all users</a>
        <div class="container section-60">
            <h2 class="page-title">Create a new user</h2>
            <form action="" method="POST" enctype="multipart/form-data">
                {$renderFileInput("Photo", "photo", "image/png, image/gif, image/jpeg")}
                {$renderTextInput("Firts Name", "firstname", "", "text", $validationErrors, true)}
                {$renderTextInput("Last Name", "lastname", "", "text", $validationErrors, true)}
                {$renderTextInput("Address", "address", "", "text", $validationErrors, true)}
                {$renderTextInput("Telephone", "phone", "", "tel", $validationErrors, true)}
                {$renderTextInput("Email", "email", "", "email", $validationErrors, true)}
                {$renderTextInput("Password", "password", "", "password", $validationErrors, true)}
                {$renderTextInput("Repeat Password", "repeatPassword", "", "password", $validationErrors, true)}
                {$renderSelectInput("Role", "role", "", $roleOptions, $validationErrors, true)}
                {$renderSelectInput("Status", "status", "", $statusOptions, $validationErrors, true)}
                <input class="btn-submit" type="submit" name="submit" value="Add User">
            </form>
        </div>
    HTML;

    echo $html;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $result = processUserRegister($connection);
    $validationErrors = $result["validationErrors"];
    $success = $result["success"];
    
    if ($success) {
        $user = $result["data"];
        header("Location: ".$_SERVER['PHP_SELF'] . "?page=manage-users");
        exit();
    }
}

renderForm($validationErrors);
?>