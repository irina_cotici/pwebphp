<?php
$connection = getConnection();
$user = null;
$validationErrors = [];

function renderForm($validationErrors) {
    $renderTextInput = 'renderTextInput';

    $html = <<<HTML
        <div class="container section-40">
            <form action="" method="POST">
                {$renderTextInput("Email", "email", "", "email", $validationErrors)}
                {$renderTextInput("Password", "password", "", "password", $validationErrors)}
                <input class="btn-submit" type="submit" name="submit" value="Login">
            </form>
    HTML;

    echo $html;
}

function renderProfile() {
    $userInfo = $_SESSION['user'];
    $currentDir = dirname($_SERVER['PHP_SELF']);
    $userPhoto = $userInfo["photo"] ? ($currentDir . $userInfo["photo"]) : PROFILE_PLACEHOLDER;

    $html = <<<HTML
    <div class="section-40 container profile">
        <h2>User Profile</h2>
        <div class="profile-info">
            <div class="profile-photo photo">
                <img src="{$userPhoto}" alt="Profile Photo" class="rounded">
            </div>
    HTML;

    if (isset($userInfo['firstname']) && isset($userInfo['lastname'])) {
        $html .= <<<HTML
            <p><strong>Name:</strong> {$userInfo['firstname']} {$userInfo['lastname']}</p>
        HTML;
    }

    if (isset($userInfo['email'])) {
        $html .= <<<HTML
            <p><strong>Email:</strong> {$userInfo['email']}</p>
        HTML;
    }

    if (isset($userInfo['username'])) {
        $html .= <<<HTML
            <p><strong>Phone:</strong> {$userInfo['phone']}</p>
        HTML;
    }

    if (isset($userInfo['role'])) {
        $userRole = USER_ROLES[$userInfo["role"]];
        $html .= <<<HTML
            <p><strong>Role:</strong> {$userRole}</p>
        HTML;
    }

    $html .= <<<HTML
        </div>
        <a class="btn-submit" href='index.php?page=user-edit&id={$userInfo["id"]}'>Edit User</a>
        <form action="web-frontend/logout.php" method="POST">
            <input class="btn-submit btn-secondary" type="submit" name="logout" value="Logout">
        </form>
    </div>
    HTML;

    echo $html;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $result = processUserLogin($connection);
    $user = $result["data"];
    $validationErrors = $result["validationErrors"];
    $success = $result["success"];
    
    if ($success && $user['status'] == 0) {
        $_SESSION['authenticated'] = true;
        $_SESSION['user'] = $user;
        header("Location: ".$_SERVER['PHP_SELF']);
    }
}

if (isset($_SESSION['authenticated']) && $_SESSION['authenticated']) {
    renderProfile();
} else {
    renderForm($validationErrors);
    if (isset($user['status']) && $user['status'] == 1) {
        $html = <<<HTML
                <div class="invalid-input">Your user might be blocked by an admin.</div>
            </div>
        HTML;
    } else {
        $html = <<<HTML
            </div>
        HTML;
    }
    echo $html;
}