<?php
$connection = getConnection();
$result = processGetAllUsers($connection);
$users = $result["data"];
$user = null;

function renderUsers($users) {
    $html = <<<HTML
        <h2 class="page-title">Manage all the users</h2>
        <a class="nav-button" href="index.php?page=add-user">+ Add a new user</a>
    HTML;

    foreach ($users as $key => $user) {
        $userStatus = USER_STATUSES[$user["status"]];
        $userRole = USER_ROLES[$user["role"]];
        $currentDir = dirname($_SERVER['PHP_SELF']);
        $userPhoto = $user["photo"] ? ($currentDir . $user["photo"]) : PROFILE_PLACEHOLDER;
       
        $html .= <<<HTML
            <div class="list-container">
                <div class="user-photo photo">
                    <img src='{$userPhoto}' alt="Profile Photo" class="rounded">
                </div>
                <div class="user-info">
                    <div>
                        <strong>ID: </strong>{$user["id"]}
                    </div>
                    <div>
                        <strong>Username: </strong>{$user["firstname"]} {$user["lastname"]}
                    </div>
                    <div>
                        <strong>Email: </strong>{$user["email"]}
                    </div>
                    <div>
                        <strong>Role: </strong>{$userRole}
                    </div>
                    <div>
                        <strong>Status: </strong>{$userStatus}
                    </div>
                </div>
                <div class="buttons-container">
                    <form method="POST">
                        <input type="text" name="id" hidden value='{$user["id"]}'>
        HTML;

        if ($user["role"] == 0) {
            $html .= <<<HTML
                <button class="btn-submit btn-secondary" type="submit" name="submit" disabled>DELETE</button>
            HTML;
        }
        else {
            $html .= <<<HTML
                <button class="btn-submit btn-secondary" type="submit" name="submit">DELETE</button>
            HTML;
        }

        $html .= <<<HTML
                </form>
        HTML;

        $html .= <<<HTML
                    <form method="POST">
                        <a class="btn-submit btn-secondary" href='index.php?page=user-edit&id={$user["id"]}'>EDIT</a>
                    </form>
                </div>
            </div>
        HTML;
    }


    $html .= <<<HTML
                    </div>
                </div>
            </div>
        </div>
    HTML;

    echo $html;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = processInput("id");

    if ($id) {
        $result = processUserDelete($connection, $id);
        $status = $result["data"];
        
        if ($status) {
            header("Location: ".$_SERVER['PHP_SELF']."?page=manage-users");
            exit();
        }
    }
}
renderUsers($users);
?>