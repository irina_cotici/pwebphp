<?php

$connection = getConnection();
$user = null;
$validationErrors = [];
$userId = "";
$success = false;

function renderForm($validationErrors, $user, $success) {
    $renderTextInput = 'renderTextInput';
    $renderFileInput = 'renderFileInput';
    $renderSelectInput = 'renderSelectInput';

    $photo = isset($user["photo"]) ? $user["photo"] : "";
    $firstname = isset($user["firstname"]) ? $user["firstname"] : "";
    $lastname = isset($user["lastname"]) ? $user["lastname"] : "";
    $address = isset($user["address"]) ? $user["address"] : "";
    $phone = isset($user["phone"]) ? $user["phone"] : "";
    $email = isset($user["email"]) ? $user["email"] : "";
    $role = isset($user["role"]) ? $user["role"] : "";
    $status = isset($user["status"]) ? $user["status"] : "";

    $roleOptions = [
        ["label" => "Admin", "value" => 0],
        ["label" => "Moderator", "value" => 1]
    ];

    $statusOptions = [
        ["label" => "Active", "value" => 0],
        ["label" => "Disabled", "value" => 1]
    ];

    // check if it self edit
    $authUser = $_SESSION['user'];
    $formTitle = $user['id'] == $_SESSION['user']['id'] ? "Edit my profile" : "Edit the user";

    $html = <<<HTML
        <a class="nav-button" href="index.php?page=manage-users">List all users</a>
        <div class="container section-60">
            <form action="" method="POST" enctype="multipart/form-data">
                <h2 class="page-title">$formTitle</h2>
                {$renderFileInput("Photo", "photo", "image/png, image/gif, image/jpeg")}
                {$renderTextInput("Firts Name", "firstname", $firstname, "text", $validationErrors)}
                {$renderTextInput("Last Name", "lastname", $lastname, "text", $validationErrors)}
                {$renderTextInput("Address", "address", $address, "text", $validationErrors)}
                {$renderTextInput("Telephone", "phone", $phone, "tel", $validationErrors)}
                {$renderTextInput("Email", "email", $email, "email", $validationErrors)}
                {$renderSelectInput("Role", "role", $role, $roleOptions, $validationErrors)}
                {$renderSelectInput("Status", "status", $status, $statusOptions, $validationErrors)}
                <input class="btn-submit" type="submit" name="submit" value="Update">
            </form>
        </div>
    HTML;

    if ($success) {
        $html .= <<<HTML
            <div class="success">User updated sucesfully</div>
        HTML;
    }

    echo $html;
}

if (isset($_GET["id"])) {
    $userId = $_GET["id"];
    $result = processGetUserById($connection, $userId);
    $user = $result["data"];
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $success = false;
    $result = processUpdateUser($connection, $user);
    $validationErrors = $result["validationErrors"];
    $success = $result["success"];
}

renderForm($validationErrors, $user, $success);
?>