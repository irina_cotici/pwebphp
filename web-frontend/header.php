<?php
function renderHeader($isAuth, $isAdmin) {
    $currentDir = dirname($_SERVER['PHP_SELF']);
    $header = <<<HTML
    <header>
        <div class="logo">
            <a href="{$currentDir}?page=home">
                <img src="{$currentDir}/assets/logo.png" alt="Logo">
            </a>
            <h1 class="header-title">Complain, don't be silent!</h1>
        </div>
        <div class="header-description">
            <p>A place where people can create their incidents.</p>
        </div>
    HTML;

    $header .= renderNavigation($isAuth, $isAdmin);

    $header .= <<<HTML
    </header>
    HTML;

    echo $header;
}

function renderNavigation($isAuth, $isAdmin) {
    $navItems = [];
    
    if (!$isAuth) {
        $navItems = [
            "home" => "Home",
            "view-incidents" => "View incidents",
        ];
    }
    else if ($isAdmin) {
        $navItems = [
            "home" => "Home",
            "view-incidents" => "View incidents",
            "create-incident" => "Create new incident",
            "my-incidents" => "My incidents",
            "manage-users" => "Manage users",
            "view-logs" => "View logs",
            "manage-database" => "Manage database"
        ];
    }
    else {
        $navItems = [
            "home" => "Home",
            "view-incidents" => "View incidents",
            "create-incident" => "Create new incident",
            "my-incidents" => "My incidents",
        ];
    }

    $currentPage = $_GET['page'] ?? '';

    $nav = '<nav><ul>';
    
    foreach ($navItems as $page => $label) {
        $isActive = $page === $currentPage ? 'active' : '';
        $nav .= '<li><a href="index.php?page=' . $page . '" class="' . $isActive . '">' . $label . '</a></li>';
    }
    
    $nav .= '</ul></nav>';

    return $nav;
}
?>