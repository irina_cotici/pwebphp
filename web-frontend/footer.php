<?php
function renderFooter() {
    $authors = "Dan Glodeanu & Irina Cotici";
    $pdfLink = dirname($_SERVER['PHP_SELF']) . "/documentation.pdf";
    $projectName = "(c) Web Technologies 2023";

    $footerContent = <<<HTML
        <footer>
            <div>Authors: $authors</div>
            <div><a href="$pdfLink" target="_blank">View PDF documentation</a></div>
            <div>$projectName</div>
        </footer>
    HTML;

    echo $footerContent;
}
?>