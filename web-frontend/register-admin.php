<?php
require '../functions.php';
require '../database/db_connection.php';
require '../web-backend/user.php';
require '../database/user.php';
require './header.php';
require './footer.php';

$connection = getConnection();
$user = null;
$error = "";

function renderForm($error) {
    $html = <<<HTML
        <form action="" method="POST" enctype="multipart/form-data">
            <input class="btn-submit" type="submit" name="submit" value="Create Admin">
    HTML;

    if ($error) {
        $html .= <<<HTML
            <div>$error</div>
        HTML;
    }

    $html .= <<<HTML
        </form>
    HTML;

    echo $html;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $result = registerFirstAdmin($connection);
    $user = $result["data"];
    $error = $result["error"];
    $success = $result["success"];

    if ($success) {

    }
}

renderHeader($user);
renderForm($error);
renderFooter();
?>