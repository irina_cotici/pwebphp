<?php

function getAllUsers($connection) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $users = [];
  $sql = "SELECT * FROM PUser";

  $result = $connection->query($sql);

  if ($result && $result->num_rows > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
      $users[] = $row;
    }
  }

  return $users;
}

function getUserById($connection, $userId) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedEmailUserId = mysqli_real_escape_string($connection, $userId);
  $sql = "SELECT * FROM PUser WHERE id = $escapedEmailUserId";

  $result = $connection->query($sql);

  if ($result && $result->num_rows > 0) {
    $user = $result->fetch_assoc();

    return $user;
  }

  return null;
}

function removeUserById($connection, $userId) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedEmailUserId = mysqli_real_escape_string($connection, $userId);
  $sql = "DELETE FROM PUser WHERE id = ?";

  $statement = $connection->prepare($sql);
  $statement->bind_param("i", $escapedEmailUserId);
  $statement->execute();

  if ($statement->affected_rows > 0) {
    return true;
  }

  return null;
}

function getUserByEmail($connection, $email) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedEmail = mysqli_real_escape_string($connection, $email);

  $sql = "SELECT * FROM PUser WHERE email = '$escapedEmail'";

  $result = $connection->query($sql);

  if ($result && $result->num_rows > 0) {
    $user = $result->fetch_assoc();

    return $user;
  }

  return null;
}

function getUser($connection, $email, $password) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedEmail = mysqli_real_escape_string($connection, $email);
  $escapedPassword = mysqli_real_escape_string($connection, $password);

  $sql = "SELECT * FROM PUser WHERE email = '$escapedEmail' AND password = '$escapedPassword'";

  $result = $connection->query($sql);

  if ($result && $result->num_rows > 0) {
    $user = $result->fetch_assoc();

    return $user;
  }

  return null;
}

function addUser(
  $connection,
  $email,
  $password,
  $firstname,
  $lastname,
  $photo,
  $address,
  $phone,
  $role,
  $status
) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedEmail = mysqli_real_escape_string($connection, $email);
  $escapedPassword = mysqli_real_escape_string($connection, $password);
  $escapedFirstname = mysqli_real_escape_string($connection, $firstname);
  $escapedLastname = mysqli_real_escape_string($connection, $lastname);
  $escapedPhoto = mysqli_real_escape_string($connection, $photo);
  $escapedAddress = mysqli_real_escape_string($connection, $address);
  $escapedPhone = mysqli_real_escape_string($connection, $phone);
  $escapedRole = mysqli_real_escape_string($connection, $role);
  $escapedStatus = mysqli_real_escape_string($connection, $status);

  $sql = "INSERT INTO PUser
    (firstname, lastname, email, photo, address, phone, password, role, status) VALUES
    ('$escapedFirstname', '$escapedLastname', '$escapedEmail', '$escapedPhoto', '$escapedAddress', '$escapedPhone', '$escapedPassword', $escapedRole, $escapedStatus)";

  $result = $connection->query($sql);

  if ($result) {
    $userId = $connection->insert_id;

    return getUserById($connection, $userId);
  }

  return null;
}

function updateUser(
  $connection,
  $userId,
  $email,
  $firstname,
  $lastname,
  $photo,
  $address,
  $phone,
  $role,
  $status
) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedEmail = mysqli_real_escape_string($connection, $email);
  $escapedUserId = mysqli_real_escape_string($connection, $userId);
  $escapedFirstname = mysqli_real_escape_string($connection, $firstname);
  $escapedLastname = mysqli_real_escape_string($connection, $lastname);
  $escapedPhoto = mysqli_real_escape_string($connection, $photo);
  $escapedAddress = mysqli_real_escape_string($connection, $address);
  $escapedPhone = mysqli_real_escape_string($connection, $phone);
  $escapedRole = mysqli_real_escape_string($connection, $role);
  $escapedStatus = mysqli_real_escape_string($connection, $status);

  $sql = "UPDATE PUser SET
    firstname='$escapedFirstname',
    lastname='$escapedLastname',
    email='$escapedEmail',
    photo='$escapedPhoto',
    address='$escapedAddress',
    phone='$escapedPhone',
    role=$escapedRole,
    status=$escapedStatus
    WHERE id=$escapedUserId";

  $result = mysqli_query($connection, $sql);

  if ($result) {
    return true;
  } else {
    return null;
  }
}
?>