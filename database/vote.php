<?php
function getAllIncidentVotes($connection, $incidentId) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedIncidentId = mysqli_real_escape_string($connection, $incidentId);
  $sql = "SELECT * FROM Vote WHERE incidentId = '$escapedIncidentId'";

  $result = $connection->query($sql);
  $photos = [];

  if ($result && $result->num_rows > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
      $photos[] = $row;
    }
  }

  return $photos;
}

function getVoteByUserId($connection, $userId, $incidentId) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedEmailUserId = mysqli_real_escape_string($connection, $userId);
  $sql = "SELECT * FROM Vote WHERE userId = $escapedEmailUserId AND incidentId = $incidentId";

  $result = $connection->query($sql);

  if ($result && $result->num_rows > 0) {
    $vote = $result->fetch_assoc();

    return $vote;
  }

  return null;
}

function getVoteByAnonymousToken($connection, $anonymousToken, $incidentId) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedAnonymousToken = mysqli_real_escape_string($connection, $anonymousToken);
  $sql = "SELECT * FROM Vote WHERE anonymousToken = '$escapedAnonymousToken' AND incidentId = $incidentId";

  $result = $connection->query($sql);

  if ($result && $result->num_rows > 0) {
    $vote = $result->fetch_assoc();

    return $vote;
  }

  return null;
}

function addVote(
  $connection,
  $isPositive,
  $anonymousToken,
  $incidentId,
  $userId
) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedIsPositive = mysqli_real_escape_string($connection, $isPositive);
  $escapedAnonymousToken = mysqli_real_escape_string($connection, $anonymousToken);
  $escapedIncidentId = mysqli_real_escape_string($connection, $incidentId);
  $escapedUserId = mysqli_real_escape_string($connection, $userId);

  $sqlIsPositive = $escapedIsPositive ? "True" : "False";
  $sql = "INSERT INTO Vote
    (isPositive, anonymousToken, incidentId, userId) VALUES
    ($sqlIsPositive, '$escapedAnonymousToken', '$escapedIncidentId', '$escapedUserId')";

  $result = $connection->query($sql);

  if ($result) {
    return true;
  }

  echo "SQL Error: " . $connection->error;

  return null;
}

function editVoteByUserId(
  $connection,
  $isPositive,
  $userId,
  $voteId
) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedIsPositive = mysqli_real_escape_string($connection, $isPositive);
  $escapedUserId = mysqli_real_escape_string($connection, $userId);
  $escapedVoteId = mysqli_real_escape_string($connection, $voteId);

  $sqlIsPositive = $escapedIsPositive ? "True" : "False";
  $sql = "UPDATE Vote SET
    isPositive=$sqlIsPositive
    WHERE userId=$escapedUserId AND id=$escapedVoteId";

  $result = $connection->query($sql);

  if ($result) {
    return true;
  }

  echo "SQL Error: " . $connection->error;

  return null;
}

function editVoteByToken(
  $connection,
  $isPositive,
  $anonymousToken,
  $voteId
) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedIsPositive = mysqli_real_escape_string($connection, $isPositive);
  $escapedAnonymousToken = mysqli_real_escape_string($connection, $anonymousToken);
  $escapedVoteId = mysqli_real_escape_string($connection, $voteId);

  $sqlIsPositive = $escapedIsPositive ? "True" : "False";
  $sql = "UPDATE Vote SET
    isPositive=$sqlIsPositive
    WHERE anonymousToken='$escapedAnonymousToken' AND id=$escapedVoteId";

  $result = $connection->query($sql);

  if ($result) {
    return true;
  }

  echo "SQL Error: " . $connection->error;

  return null;
}
?>