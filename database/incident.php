<?php

function getAllIncidents($connection) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $users = [];
  $sql = "SELECT * FROM Incident";

  $result = $connection->query($sql);

  if ($result && $result->num_rows > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
      $users[] = $row;
    }
  }

  return $users;
}

function getUserIncidents($connection, $userId) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedUserId = mysqli_real_escape_string($connection, $userId);
  $incidents = [];
  $sql = "SELECT * FROM Incident WHERE userId = $escapedUserId";

  $result = $connection->query($sql);

  if ($result && $result->num_rows > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
      $incidents[] = $row;
    }
  }

  return $incidents;
}

function getIncidentById($connection, $incidentId) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedincidentId = mysqli_real_escape_string($connection, $incidentId);
  $sql = "SELECT * FROM Incident WHERE id = $escapedincidentId";

  $result = $connection->query($sql);

  if ($result && $result->num_rows > 0) {
    $user = $result->fetch_assoc();

    return $user;
  }

  return null;
}

function removeIncidentById($connection, $incidentId) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedEmailIncidentId = mysqli_real_escape_string($connection, $incidentId);
  $sql = "DELETE FROM Incident WHERE id = ?";

  $statement = $connection->prepare($sql);
  $statement->bind_param("i", $escapedEmailIncidentId);
  $statement->execute();

  if ($statement->affected_rows > 0) {
    return true;
  }

  return null;
}

function addIncident(
  $connection,
  $title,
  $description,
  $address,
  $keywords,
  $userId
) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedTitle = mysqli_real_escape_string($connection, $title);
  $escapedDescription = mysqli_real_escape_string($connection, $description);
  $escapedAddress = mysqli_real_escape_string($connection, $address);
  $escapedkeyWords = mysqli_real_escape_string($connection, $keywords);
  $escapedkeyWords = preg_replace('/\s+/', ',', $escapedkeyWords);
  $timestamp = date("Y-m-d H:i:s");
  $status = 0;

  $sql = "INSERT INTO Incident (title, description, address, keywords, timestamp, userId, status)
    VALUES ('$escapedTitle', '$escapedDescription', '$escapedAddress', '$escapedkeyWords', '$timestamp', '$userId', '$status')";

  $result = $connection->query($sql);

  if ($result) {
    return $result;
  }

  return null;
}

function updateIncident(
  $connection,
  $title,
  $description,
  $address,
  $keywords,
  $status,
  $incidentId
) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedTitle = mysqli_real_escape_string($connection, $title);
  $escapedDescription = mysqli_real_escape_string($connection, $description);
  $escapedAddress = mysqli_real_escape_string($connection, $address);
  $escapedStatus = mysqli_real_escape_string($connection, $status);
  $escapedkeyWords = mysqli_real_escape_string($connection, $keywords);
  $escapedkeyWords = preg_replace('/\s+/', ',', $escapedkeyWords);

  $sql = "UPDATE Incident SET
    title='$escapedTitle',
    description='$escapedDescription',
    address='$escapedAddress',
    keywords='$escapedkeyWords',
    status=$escapedStatus
    WHERE id='$incidentId'
  ";

  $result = mysqli_query($connection, $sql);

  if ($result) {
    return $result;
  }

  return null;
}

?>