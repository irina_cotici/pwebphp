<?php
function getAllIncidentComments($connection, $incidentId) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedIncidentId = mysqli_real_escape_string($connection, $incidentId);
  $sql = "SELECT Comment.*, PUser.firstname, PUser.lastname
    FROM Comment
    LEFT JOIN PUser ON Comment.userId = PUser.id
    WHERE incidentId = '$escapedIncidentId'
    ORDER BY Comment.timestamp DESC
  ";

  $result = $connection->query($sql);
  $comments = [];

  if ($result && $result->num_rows > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
      $comments[] = $row;
    }
  }

  return $comments;
}

function addComment(
  $connection,
  $description,
  $timestamp,
  $incidentId,
  $userId
) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedDescription = mysqli_real_escape_string($connection, $description);
  $escapedTimestamp = mysqli_real_escape_string($connection, $timestamp);
  $escapedIncidentId = mysqli_real_escape_string($connection, $incidentId);
  $escapedUserId = mysqli_real_escape_string($connection, $userId);

  $sql = "INSERT INTO Comment
    (description, timestamp, incidentId, userId) VALUES
    ('$escapedDescription', '$escapedTimestamp', '$escapedIncidentId', '$escapedUserId')";

  $result = $connection->query($sql);

  if ($result) {
    return true;
  }
  echo "SQL Error: " . $connection->error;

  return null;
}
?>