<?php
function getAllLogs($connection) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $sql = "SELECT Log.*, PUser.firstname, PUser.lastname
    FROM Log
    LEFT JOIN PUser ON Log.userId = PUser.id
    ORDER BY Log.timestamp DESC
  ";

  $result = $connection->query($sql);
  $photos = [];

  if ($result && $result->num_rows > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
      $photos[] = $row;
    }
  }

  return $photos;
}

function addLog(
  $connection,
  $message,
  $timestamp,
  $userId
) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedMessage = mysqli_real_escape_string($connection, $message);
  $escapedTimestamp = mysqli_real_escape_string($connection, $timestamp);
  $escapedUserId = mysqli_real_escape_string($connection, $userId);

  $sql = "INSERT INTO Log
    (message, timestamp, userId) VALUES
    ('$escapedMessage', '$escapedTimestamp', '$escapedUserId')";

  $result = $connection->query($sql);

  if ($result) {
    return true;
  }

  echo "SQL Error: " . $connection->error;

  return null;
}
?>