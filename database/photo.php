<?php
function getAllIncidentPhotos($connection, $incidentId) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedIncidentId = mysqli_real_escape_string($connection, $incidentId);
  $sql = "SELECT * FROM IncidentPhoto WHERE incidentId = '$escapedIncidentId'";

  $result = $connection->query($sql);
  $photos = [];

  if ($result && $result->num_rows > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
      $photos[] = $row;
    }
  }

  return $photos;
}

function removePhotoById($connection, $photoId) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedPhotoId = mysqli_real_escape_string($connection, $photoId);
  $sql = "DELETE FROM IncidentPhoto WHERE id = ?";

  $statement = $connection->prepare($sql);
  $statement->bind_param("i", $escapedPhotoId);
  $statement->execute();

  if ($statement->affected_rows > 0) {
    return true;
  }

  return null;
}


function addPhoto(
  $connection,
  $photo,
  $incidentId
) {

  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  $escapedPhoto = mysqli_real_escape_string($connection, $photo);
  $escapedIncidentId = mysqli_real_escape_string($connection, $incidentId);

  $sql = "INSERT INTO IncidentPhoto
    (photo, incidentId) VALUES
    ('$escapedPhoto', '$escapedIncidentId')";

  $result = $connection->query($sql);

  if ($result) {
    $userId = $connection->insert_id;

    return getUserById($connection, $userId);
  }

  echo "SQL Error: " . $connection->error;

  return null;
}
?>