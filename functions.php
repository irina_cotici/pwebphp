<?php

function sanitizeInput($input) {
  return $input;
}

function validateInput($inputName, $inputValue, $validations, $validationErrors=[]) {

  if (!$validations) {
    return $validationErrors;
  }

  $validationErrors[$inputName] = [];

  if (isset($validations["required"]) && !$inputValue && $inputValue != "0") {
    $validationErrors[$inputName]["required"] = "This field is required";
  }
  if (isset($validations["email"]) && !filter_var($inputValue, FILTER_VALIDATE_EMAIL)) {
    $validationErrors[$inputName]["email"] = "This field should be a valid email";
  }
  if (isset($validations["phone"]) && preg_match("/^\+?[0-9]{1,}$/", !$inputValue)) {
    $validationErrors[$inputName]["phone"] = "This field should be a valid phone number";
  }
  if (isset($validations["min"]) && strlen($inputValue) < $validations["min"]) {
    $validationErrors[$inputName]["min"] = "This field should have at least " . $validations["min"] . " characters";
  }
  if (isset($validations["max"]) && strlen($inputValue) > $validations["max"]) {
    $validationErrors[$inputName]["max"] = "This field should have at most " . $validations["max"] . " characters";
  }
  if (isset($validations["number"]) && !is_numeric($inputValue)) {
    $validationErrors[$inputName]["number"] = "This field should be a number";
  }
  if (isset($validations["match"]) && $inputValue != $validations["match"]["value"]) {
    $validationErrors[$inputName]["match"] = "This field should match " . $validations["match"]["fieldName"];
  }

  if (count($validationErrors[$inputName]) == 0) {
    unset($validationErrors[$inputName]);
  }

  return $validationErrors;
}

function printValidationsErrors($validationErrors) {
  foreach ($validationErrors as $key => $value) {
    echo $key . ":" . "<br>";
    foreach ($value as $key1 => $value1) {
      echo $key . "-" . $key1 . ": " . $value1 . "<br>";
    }
    echo "<br>";
  }
}

function processInput($name) {
  if (isset($_POST[$name])) {
    $value = $_POST[$name];
    return trim(stripcslashes((htmlspecialchars($value))));
  }
  else {
    return '';
  }
}

function processFile($name) {
  if (isset($_FILES[$name])) {
    $file = $_FILES[$name];
    return $file;
  }
  else {
    return null;
  }
}

function getInputErrors($validationErrors) {
  $html = <<<HTML
  HTML;

  if (isset($validationErrors)) {
    foreach ($validationErrors as $key => $error) {
      $html .= <<<HTML
        <div class="invalid-input">$error</div>
      HTML;
    }
  }

  return $html;
}

function renderTextInput($label, $identifier, $value="", $type="text", $validationErrors=null, $preserveValue=false) {
  if ($preserveValue && isset($_POST[$identifier])) {
    $value = $_POST[$identifier];
  }
  
  $html = <<<HTML
    <label for="$identifier">$label</label>
    <input type="$type" value="$value" name="$identifier" id="$identifier"/>
  HTML;

  if (isset($validationErrors[$identifier])) {
    $html .= getInputErrors($validationErrors[$identifier]);
  }

  return $html;
}

function  renderFlagInput($identifier, $value="") {
  $html = <<<HTML
    <input type="text" name="$identifier" id="$identifier" hidden value="$value">
  HTML;

  return $html;
}

function renderTextArea($label, $identifier, $value="", $rows="6", $validationErrors=null, $preserveValue=false) {
  if ($preserveValue && isset($_POST[$identifier])) {
    $value = $_POST[$identifier];
  }

  $textareaText = $value ? $value : '';
  
  $html = <<<HTML
    <label for="$identifier">$label</label>
    <textarea rows="$rows" name="$identifier" id="$identifier">$textareaText</textarea>
  HTML;

  if (isset($validationErrors[$identifier])) {
    $html .= getInputErrors($validationErrors[$identifier]);
  }

  return $html;
}

function renderFileInput($label, $identifier, $accept=null) {
  $html = <<<HTML
    <label for="$identifier">$label</label>
    <input type="file" name="$identifier" id="$identifier" accept="$accept"/>
  HTML;

  return $html;
}

function renderSelectInput($label, $identifier, $selectValue="", $options=[], $validationErrors=null, $preserveValue=false) {
  if ($preserveValue && isset($_POST[$identifier])) {
    $selectValue = $_POST[$identifier];
  }

  $html = <<<HTML
    <label for="$identifier">$label</label>
    <select name="$identifier" id="$identifier">
  HTML;

  foreach ($options as $option) {
    $label = $option['label'];
    $value = $option['value'];

    if ($selectValue == $value) {
      $html .= <<<HTML
        <option selected value="$value">$label</option>
      HTML;
    }
    else {
      $html .= <<<HTML
        <option value="$value">$label</option>
      HTML;
    }
    
  }

  $html .= <<<HTML
    </select>
  HTML;

  if (isset($validationErrors[$identifier])) {
    $html .= getInputErrors($validationErrors[$identifier]);
  }

  return $html;
}

function checkRedirect() {
  if (!isset($_SESSION['authenticated'])) {
		header('Location: index.php');
		exit;
	}
}
?>