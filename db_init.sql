-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Gazdă: 127.0.0.1:3306
-- Timp de generare: iul. 05, 2023 la 02:32 PM
-- Versiune server: 8.0.31
-- Versiune PHP: 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Bază de date: `pwebdb`
--

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel Comment
--

DROP TABLE IF EXISTS Comment;
CREATE TABLE IF NOT EXISTS Comment (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `incidentId` int DEFAULT NULL,
  `userId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `incidentId` (`incidentId`),
  KEY `userId` (`userId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Eliminarea datelor din tabel Comment
--

INSERT INTO Comment (`id`, `description`, `timestamp`, `incidentId`, `userId`) VALUES
(1, 'I really hope it will be fixed soon!', '2023-07-05 10:12:02', 1, 1),
(2, 'I agree', '2023-07-05 10:23:47', 1, 0),
(3, 'Let\'s solve this problem together!', '2023-07-05 10:34:24', 3, 0);

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel Incident
--

DROP TABLE IF EXISTS Incident;
CREATE TABLE IF NOT EXISTS Incident (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `userId` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Eliminarea datelor din tabel Incident
--

INSERT INTO Incident (`id`, `title`, `description`, `keywords`, `timestamp`, `userId`, `status`, `address`) VALUES
(1, 'Trash in the Park', 'Firstly, the sight of trash strewn across the park creates an unpleasant atmosphere for visitors. The park is meant to be a peaceful and serene space where individuals can connect with nature, but the presence of litter disrupts this tranquility. It diminishes the overall experience and detracts from the park\'s aesthetic appeal, ultimately discouraging people from visiting or spending quality time outdoors.\r\n\r\nMoreover, the accumulation of trash can have severe environmental consequences. Plastic bags, bottles, and other non-biodegradable items not only take years to decompose but can also harm wildlife when ingested or entangled. Our park boasts a rich biodiversity, and it is disheartening to witness the potential impact of this litter on the delicate ecosystem and the animals that call it home.', 'park,trash', '2023-07-05 10:11:43', 1, 2, 'Central Park'),
(2, 'Traffic Light', 'I am writing to bring your attention to a critical issue concerning a non-functioning traffic light at the intersection of Liberty Street and Tree Street. As a concerned citizen, I have observed that the traffic signal at this intersection has been consistently malfunctioning, leading to significant traffic disruptions and safety hazards.\r\n\r\nThe malfunctioning traffic light poses several risks to both motorists and pedestrians. Without clear instructions from a functioning traffic signal, drivers are left to navigate the intersection based solely on their own judgment, resulting in confusion and potential accidents. This situation is particularly hazardous during peak traffic hours when the intersection experiences heavy congestion.', 'traffic,accidents,danger', '2023-07-05 10:26:20', 1, 0, 'Liberty Street and Tree Street intersection'),
(3, 'Stray Animals', 'I am writing to propose the establishment of a dedicated center for homeless animals in our community. As a passionate advocate for animal welfare, I have observed the increasing number of stray and abandoned animals within our area, and I believe it is our responsibility to provide them with a safe haven and an opportunity for a better life.', 'animals,center,stray', '2023-07-05 10:32:35', 2, 0, 'Granada');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel IncidentPhoto
--

DROP TABLE IF EXISTS IncidentPhoto;
CREATE TABLE IF NOT EXISTS IncidentPhoto (
  `id` int NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incidentId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `incidentId` (`incidentId`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Eliminarea datelor din tabel IncidentPhoto
--

INSERT INTO IncidentPhoto (`id`, `photo`, `incidentId`) VALUES
(1, '/assets/profile/1688552042_b9a839b0bc743bbb.jpg', 1),
(2, '/assets/profile/1688552047_cbdb16328af520d8.jpg', 1),
(3, '/assets/profile/1688552817_cb420d31009a3433.jpg', 2),
(4, '/assets/profile/1688553203_104ab843c9826ebe.jpg', 3),
(5, '/assets/profile/1688553209_28902edbe5513482.jpg', 3);

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel Log
--

DROP TABLE IF EXISTS Log;
CREATE TABLE IF NOT EXISTS Log (
  `id` int NOT NULL AUTO_INCREMENT,
  `userId` int DEFAULT NULL,
  `message` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`)
) ENGINE=MyISAM AUTO_INCREMENT=259 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Eliminarea datelor din tabel Log
--

INSERT INTO Log (`id`, `userId`, `message`, `timestamp`) VALUES
(1, 0, 'Got all incidents', '2023-07-05 09:56:30'),
(2, 0, 'Got all incidents', '2023-07-05 09:56:31'),
(3, 0, 'Logged in in the system', '2023-07-05 10:10:09'),
(4, 1, 'Got all incidents', '2023-07-05 10:10:18'),
(5, 1, 'Added a incident', '2023-07-05 10:11:43'),
(6, 1, 'Got all incidents', '2023-07-05 10:11:44'),
(7, 1, 'Got all incident\'s votes', '2023-07-05 10:11:44'),
(8, 1, 'Got all incidents', '2023-07-05 10:11:48'),
(9, 1, 'Got all incident\'s votes', '2023-07-05 10:11:48'),
(10, 1, 'Voted for an incident', '2023-07-05 10:11:48'),
(11, 1, 'Got all incidents', '2023-07-05 10:11:48'),
(12, 1, 'Got all incident\'s votes', '2023-07-05 10:11:48'),
(13, 1, 'Got all incident\'s photos', '2023-07-05 10:11:51'),
(14, 1, 'Got all comments', '2023-07-05 10:11:51'),
(15, 1, 'Got an incident', '2023-07-05 10:11:51'),
(16, 1, 'Got all incident\'s votes', '2023-07-05 10:11:51'),
(17, 1, 'Got all incident\'s photos', '2023-07-05 10:12:02'),
(18, 1, 'Got all comments', '2023-07-05 10:12:02'),
(19, 1, 'Got an incident', '2023-07-05 10:12:02'),
(20, 1, 'Got all incident\'s votes', '2023-07-05 10:12:02'),
(21, 1, 'Added a comment', '2023-07-05 10:12:02'),
(22, 1, 'Got all comments', '2023-07-05 10:12:02'),
(23, 1, 'Got all incidents', '2023-07-05 10:12:06'),
(24, 1, 'Got all incident\'s votes', '2023-07-05 10:12:06'),
(25, 1, 'Got an incident', '2023-07-05 10:12:09'),
(26, 1, 'Got all incident\'s photos', '2023-07-05 10:12:09'),
(27, 1, 'Got an incident', '2023-07-05 10:14:02'),
(28, 1, 'Got all incident\'s photos', '2023-07-05 10:14:02'),
(29, 1, 'Added a photo to an incident', '2023-07-05 10:14:02'),
(30, 1, 'Got all incident\'s photos', '2023-07-05 10:14:02'),
(31, 1, 'Got an incident', '2023-07-05 10:14:07'),
(32, 1, 'Got all incident\'s photos', '2023-07-05 10:14:07'),
(33, 1, 'Added a photo to an incident', '2023-07-05 10:14:07'),
(34, 1, 'Got all incident\'s photos', '2023-07-05 10:14:07'),
(35, 1, 'Got all incidents', '2023-07-05 10:14:15'),
(36, 1, 'Got all incident\'s votes', '2023-07-05 10:14:15'),
(37, 1, 'Got all incident\'s photos', '2023-07-05 10:14:18'),
(38, 1, 'Got all comments', '2023-07-05 10:14:18'),
(39, 1, 'Got an incident', '2023-07-05 10:14:18'),
(40, 1, 'Got all incident\'s votes', '2023-07-05 10:14:18'),
(41, 1, 'Got his incidents', '2023-07-05 10:14:45'),
(42, 1, 'Got all incident\'s votes', '2023-07-05 10:14:45'),
(43, 1, 'Got all users', '2023-07-05 10:14:49'),
(44, 1, 'Added a User', '2023-07-05 10:16:31'),
(45, 1, 'Got all users', '2023-07-05 10:16:31'),
(46, 0, 'Logged in in the system', '2023-07-05 10:16:58'),
(47, 2, 'Got all incidents', '2023-07-05 10:17:11'),
(48, 2, 'Got all incident\'s votes', '2023-07-05 10:17:11'),
(49, 2, 'Got his incidents', '2023-07-05 10:17:14'),
(50, 2, 'Got all users', '2023-07-05 10:17:19'),
(51, 2, 'Got his incidents', '2023-07-05 10:17:30'),
(52, 2, 'Got all incidents', '2023-07-05 10:17:33'),
(53, 2, 'Got all incident\'s votes', '2023-07-05 10:17:33'),
(54, 2, 'Got all users', '2023-07-05 10:17:37'),
(55, 1, 'Got all users', '2023-07-05 10:21:15'),
(56, 2, 'Got all users', '2023-07-05 10:21:56'),
(57, 0, 'Got all incidents', '2023-07-05 10:22:24'),
(58, 0, 'Got all incident\'s votes', '2023-07-05 10:22:24'),
(59, 0, 'Got all incidents', '2023-07-05 10:22:32'),
(60, 0, 'Got all incident\'s votes', '2023-07-05 10:22:32'),
(61, 0, 'Voted for an incident', '2023-07-05 10:22:32'),
(62, 0, 'Got all incidents', '2023-07-05 10:22:32'),
(63, 0, 'Got all incident\'s votes', '2023-07-05 10:22:32'),
(64, 0, 'Got all incidents', '2023-07-05 10:22:33'),
(65, 0, 'Got all incident\'s votes', '2023-07-05 10:22:33'),
(66, 0, 'Got all incidents', '2023-07-05 10:22:33'),
(67, 0, 'Got all incident\'s votes', '2023-07-05 10:22:33'),
(68, 0, 'Got all incidents', '2023-07-05 10:22:34'),
(69, 0, 'Got all incident\'s votes', '2023-07-05 10:22:34'),
(70, 0, 'Got all incidents', '2023-07-05 10:22:34'),
(71, 0, 'Got all incident\'s votes', '2023-07-05 10:22:34'),
(72, 0, 'Got all incidents', '2023-07-05 10:22:39'),
(73, 0, 'Got all incident\'s votes', '2023-07-05 10:22:39'),
(74, 0, 'Got all incidents', '2023-07-05 10:22:39'),
(75, 0, 'Got all incident\'s votes', '2023-07-05 10:22:39'),
(76, 0, 'Got all incidents', '2023-07-05 10:22:40'),
(77, 0, 'Got all incident\'s votes', '2023-07-05 10:22:40'),
(78, 0, 'Got all incidents', '2023-07-05 10:22:40'),
(79, 0, 'Got all incident\'s votes', '2023-07-05 10:22:40'),
(80, 0, 'Got all incidents', '2023-07-05 10:22:40'),
(81, 0, 'Got all incident\'s votes', '2023-07-05 10:22:40'),
(82, 0, 'Got all incidents', '2023-07-05 10:22:40'),
(83, 0, 'Got all incident\'s votes', '2023-07-05 10:22:40'),
(84, 0, 'Got all incidents', '2023-07-05 10:22:42'),
(85, 0, 'Got all incident\'s votes', '2023-07-05 10:22:42'),
(86, 0, 'Got all incidents', '2023-07-05 10:22:42'),
(87, 0, 'Got all incident\'s votes', '2023-07-05 10:22:42'),
(88, 0, 'Got all incidents', '2023-07-05 10:22:43'),
(89, 0, 'Got all incident\'s votes', '2023-07-05 10:22:43'),
(90, 0, 'Got all incidents', '2023-07-05 10:22:43'),
(91, 0, 'Got all incident\'s votes', '2023-07-05 10:22:43'),
(92, 0, 'Got all incidents', '2023-07-05 10:22:44'),
(93, 0, 'Got all incident\'s votes', '2023-07-05 10:22:44'),
(94, 0, 'Got all incidents', '2023-07-05 10:22:44'),
(95, 0, 'Got all incident\'s votes', '2023-07-05 10:22:44'),
(96, 0, 'Got all incidents', '2023-07-05 10:22:44'),
(97, 0, 'Got all incident\'s votes', '2023-07-05 10:22:44'),
(98, 0, 'Got all incidents', '2023-07-05 10:22:44'),
(99, 0, 'Got all incident\'s votes', '2023-07-05 10:22:44'),
(100, 0, 'Got all incidents', '2023-07-05 10:22:45'),
(101, 0, 'Got all incident\'s votes', '2023-07-05 10:22:45'),
(102, 0, 'Got all incidents', '2023-07-05 10:22:45'),
(103, 0, 'Got all incident\'s votes', '2023-07-05 10:22:45'),
(104, 0, 'Got all incidents', '2023-07-05 10:23:22'),
(105, 0, 'Got all incident\'s votes', '2023-07-05 10:23:22'),
(106, 0, 'Got all incidents', '2023-07-05 10:23:22'),
(107, 0, 'Got all incident\'s votes', '2023-07-05 10:23:22'),
(108, 0, 'Got all incidents', '2023-07-05 10:23:23'),
(109, 0, 'Got all incident\'s votes', '2023-07-05 10:23:23'),
(110, 0, 'Got all incidents', '2023-07-05 10:23:23'),
(111, 0, 'Got all incident\'s votes', '2023-07-05 10:23:23'),
(112, 0, 'Got all incidents', '2023-07-05 10:23:24'),
(113, 0, 'Got all incident\'s votes', '2023-07-05 10:23:24'),
(114, 0, 'Got all incidents', '2023-07-05 10:23:24'),
(115, 0, 'Got all incident\'s votes', '2023-07-05 10:23:24'),
(116, 0, 'Got all incidents', '2023-07-05 10:23:24'),
(117, 0, 'Got all incident\'s votes', '2023-07-05 10:23:24'),
(118, 0, 'Got all incidents', '2023-07-05 10:23:24'),
(119, 0, 'Got all incident\'s votes', '2023-07-05 10:23:24'),
(120, 0, 'Got all incidents', '2023-07-05 10:23:35'),
(121, 0, 'Got all incident\'s votes', '2023-07-05 10:23:35'),
(122, 0, 'Got all incidents', '2023-07-05 10:23:35'),
(123, 0, 'Got all incident\'s votes', '2023-07-05 10:23:35'),
(124, 0, 'Got all incident\'s photos', '2023-07-05 10:23:43'),
(125, 0, 'Got all comments', '2023-07-05 10:23:43'),
(126, 0, 'Got an incident', '2023-07-05 10:23:43'),
(127, 0, 'Got all incident\'s votes', '2023-07-05 10:23:43'),
(128, 0, 'Got all incident\'s photos', '2023-07-05 10:23:47'),
(129, 0, 'Got all comments', '2023-07-05 10:23:47'),
(130, 0, 'Got an incident', '2023-07-05 10:23:47'),
(131, 0, 'Got all incident\'s votes', '2023-07-05 10:23:47'),
(132, 0, 'Added a comment', '2023-07-05 10:23:47'),
(133, 0, 'Got all comments', '2023-07-05 10:23:47'),
(134, 1, 'Got all incidents', '2023-07-05 10:24:15'),
(135, 1, 'Got all incident\'s votes', '2023-07-05 10:24:15'),
(136, 1, 'Got an incident', '2023-07-05 10:24:17'),
(137, 1, 'Got all incident\'s photos', '2023-07-05 10:24:17'),
(138, 1, 'Got an incident', '2023-07-05 10:24:22'),
(139, 1, 'Got all incident\'s photos', '2023-07-05 10:24:22'),
(140, 1, 'Edited an incident', '2023-07-05 10:24:22'),
(141, 1, 'Got all incidents', '2023-07-05 10:24:23'),
(142, 1, 'Got all incident\'s votes', '2023-07-05 10:24:23'),
(143, 1, 'Added a incident', '2023-07-05 10:26:20'),
(144, 1, 'Got all incidents', '2023-07-05 10:26:44'),
(145, 1, 'Got all incident\'s votes', '2023-07-05 10:26:44'),
(146, 1, 'Got all incident\'s votes', '2023-07-05 10:26:44'),
(147, 1, 'Got all incidents', '2023-07-05 10:26:47'),
(148, 1, 'Got all incident\'s votes', '2023-07-05 10:26:47'),
(149, 1, 'Got all incident\'s votes', '2023-07-05 10:26:47'),
(150, 1, 'Got all incidents', '2023-07-05 10:26:47'),
(151, 1, 'Got all incident\'s votes', '2023-07-05 10:26:47'),
(152, 1, 'Got all incident\'s votes', '2023-07-05 10:26:47'),
(153, 1, 'Got all incidents', '2023-07-05 10:26:48'),
(154, 1, 'Got all incident\'s votes', '2023-07-05 10:26:48'),
(155, 1, 'Got all incident\'s votes', '2023-07-05 10:26:48'),
(156, 1, 'Voted for an incident', '2023-07-05 10:26:48'),
(157, 1, 'Got all incidents', '2023-07-05 10:26:48'),
(158, 1, 'Got all incident\'s votes', '2023-07-05 10:26:48'),
(159, 1, 'Got all incident\'s votes', '2023-07-05 10:26:48'),
(160, 1, 'Got an incident', '2023-07-05 10:26:51'),
(161, 1, 'Got all incident\'s photos', '2023-07-05 10:26:51'),
(162, 1, 'Got an incident', '2023-07-05 10:26:52'),
(163, 1, 'Got all incident\'s photos', '2023-07-05 10:26:52'),
(164, 1, 'Got all incident\'s photos', '2023-07-05 10:26:52'),
(165, 1, 'Got an incident', '2023-07-05 10:26:57'),
(166, 1, 'Got all incident\'s photos', '2023-07-05 10:26:57'),
(167, 1, 'Added a photo to an incident', '2023-07-05 10:26:57'),
(168, 1, 'Got all incident\'s photos', '2023-07-05 10:26:57'),
(169, 1, 'Got all incidents', '2023-07-05 10:27:02'),
(170, 1, 'Got all incident\'s votes', '2023-07-05 10:27:02'),
(171, 1, 'Got all incident\'s votes', '2023-07-05 10:27:02'),
(172, 1, 'Got all incident\'s photos', '2023-07-05 10:27:05'),
(173, 1, 'Got all comments', '2023-07-05 10:27:05'),
(174, 1, 'Got an incident', '2023-07-05 10:27:05'),
(175, 1, 'Got all incident\'s votes', '2023-07-05 10:27:05'),
(176, 0, 'Logged in in the system', '2023-07-05 10:27:33'),
(177, 2, 'Got all incidents', '2023-07-05 10:27:35'),
(178, 2, 'Got all incident\'s votes', '2023-07-05 10:27:35'),
(179, 2, 'Got all incident\'s votes', '2023-07-05 10:27:35'),
(180, 2, 'Added a incident', '2023-07-05 10:32:35'),
(181, 2, 'Got his incidents', '2023-07-05 10:32:36'),
(182, 2, 'Got all incident\'s votes', '2023-07-05 10:32:36'),
(183, 2, 'Got an incident', '2023-07-05 10:32:38'),
(184, 2, 'Got all incident\'s photos', '2023-07-05 10:32:38'),
(185, 2, 'Got an incident', '2023-07-05 10:33:23'),
(186, 2, 'Got all incident\'s photos', '2023-07-05 10:33:23'),
(187, 2, 'Added a photo to an incident', '2023-07-05 10:33:23'),
(188, 2, 'Got all incident\'s photos', '2023-07-05 10:33:23'),
(189, 2, 'Got an incident', '2023-07-05 10:33:29'),
(190, 2, 'Got all incident\'s photos', '2023-07-05 10:33:29'),
(191, 2, 'Added a photo to an incident', '2023-07-05 10:33:29'),
(192, 2, 'Got all incident\'s photos', '2023-07-05 10:33:29'),
(193, 2, 'Got his incidents', '2023-07-05 10:33:36'),
(194, 2, 'Got all incident\'s votes', '2023-07-05 10:33:36'),
(195, 2, 'Got all incidents', '2023-07-05 10:33:39'),
(196, 2, 'Got all incident\'s votes', '2023-07-05 10:33:39'),
(197, 2, 'Got all incident\'s votes', '2023-07-05 10:33:39'),
(198, 2, 'Got all incident\'s votes', '2023-07-05 10:33:39'),
(199, 2, 'Got all incidents', '2023-07-05 10:33:42'),
(200, 2, 'Got all incident\'s votes', '2023-07-05 10:33:42'),
(201, 2, 'Got all incident\'s votes', '2023-07-05 10:33:42'),
(202, 2, 'Got all incident\'s votes', '2023-07-05 10:33:42'),
(203, 2, 'Voted for an incident', '2023-07-05 10:33:42'),
(204, 2, 'Got all incidents', '2023-07-05 10:33:42'),
(205, 2, 'Got all incident\'s votes', '2023-07-05 10:33:42'),
(206, 2, 'Got all incident\'s votes', '2023-07-05 10:33:42'),
(207, 2, 'Got all incident\'s votes', '2023-07-05 10:33:42'),
(208, 1, 'Got all incidents', '2023-07-05 10:33:54'),
(209, 1, 'Got all incident\'s votes', '2023-07-05 10:33:54'),
(210, 1, 'Got all incident\'s votes', '2023-07-05 10:33:54'),
(211, 1, 'Got all incident\'s votes', '2023-07-05 10:33:54'),
(212, 1, 'Got all incidents', '2023-07-05 10:33:57'),
(213, 1, 'Got all incident\'s votes', '2023-07-05 10:33:57'),
(214, 1, 'Got all incident\'s votes', '2023-07-05 10:33:57'),
(215, 1, 'Got all incident\'s votes', '2023-07-05 10:33:57'),
(216, 1, 'Voted for an incident', '2023-07-05 10:33:57'),
(217, 1, 'Got all incidents', '2023-07-05 10:33:57'),
(218, 1, 'Got all incident\'s votes', '2023-07-05 10:33:57'),
(219, 1, 'Got all incident\'s votes', '2023-07-05 10:33:57'),
(220, 1, 'Got all incident\'s votes', '2023-07-05 10:33:57'),
(221, 1, 'Got all incidents', '2023-07-05 10:34:01'),
(222, 1, 'Got all incident\'s votes', '2023-07-05 10:34:01'),
(223, 1, 'Got all incident\'s votes', '2023-07-05 10:34:01'),
(224, 1, 'Got all incident\'s votes', '2023-07-05 10:34:01'),
(225, 1, 'Got all incidents', '2023-07-05 10:34:01'),
(226, 1, 'Got all incident\'s votes', '2023-07-05 10:34:01'),
(227, 1, 'Got all incident\'s votes', '2023-07-05 10:34:01'),
(228, 1, 'Got all incident\'s votes', '2023-07-05 10:34:01'),
(229, 0, 'Got all incidents', '2023-07-05 10:34:10'),
(230, 0, 'Got all incident\'s votes', '2023-07-05 10:34:10'),
(231, 0, 'Got all incident\'s votes', '2023-07-05 10:34:10'),
(232, 0, 'Got all incident\'s votes', '2023-07-05 10:34:10'),
(233, 0, 'Got all incidents', '2023-07-05 10:34:13'),
(234, 0, 'Got all incident\'s votes', '2023-07-05 10:34:13'),
(235, 0, 'Got all incident\'s votes', '2023-07-05 10:34:13'),
(236, 0, 'Got all incident\'s votes', '2023-07-05 10:34:13'),
(237, 0, 'Voted for an incident', '2023-07-05 10:34:13'),
(238, 0, 'Got all incidents', '2023-07-05 10:34:13'),
(239, 0, 'Got all incident\'s votes', '2023-07-05 10:34:13'),
(240, 0, 'Got all incident\'s votes', '2023-07-05 10:34:13'),
(241, 0, 'Got all incident\'s votes', '2023-07-05 10:34:13'),
(242, 0, 'Got all incident\'s photos', '2023-07-05 10:34:14'),
(243, 0, 'Got all comments', '2023-07-05 10:34:14'),
(244, 0, 'Got an incident', '2023-07-05 10:34:14'),
(245, 0, 'Got all incident\'s votes', '2023-07-05 10:34:14'),
(246, 0, 'Got all incident\'s photos', '2023-07-05 10:34:24'),
(247, 0, 'Got all comments', '2023-07-05 10:34:24'),
(248, 0, 'Got an incident', '2023-07-05 10:34:24'),
(249, 0, 'Got all incident\'s votes', '2023-07-05 10:34:24'),
(250, 0, 'Added a comment', '2023-07-05 10:34:24'),
(251, 0, 'Got all comments', '2023-07-05 10:34:24'),
(252, 1, 'Got his incidents', '2023-07-05 10:35:06'),
(253, 1, 'Got all incident\'s votes', '2023-07-05 10:35:06'),
(254, 1, 'Got all incident\'s votes', '2023-07-05 10:35:06'),
(255, 1, 'Got all incidents', '2023-07-05 10:35:09'),
(256, 1, 'Got all incident\'s votes', '2023-07-05 10:35:09'),
(257, 1, 'Got all incident\'s votes', '2023-07-05 10:35:09'),
(258, 1, 'Got all incident\'s votes', '2023-07-05 10:35:09');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel PUser
--

DROP TABLE IF EXISTS PUser;
CREATE TABLE IF NOT EXISTS PUser (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Eliminarea datelor din tabel PUser
--

INSERT INTO PUser (`id`, `firstname`, `lastname`, `email`, `photo`, `address`, `phone`, `password`, `role`, `status`) VALUES
(1, 'Admin', 'Admin', 'admin@gmail.com', '', '', '', '$2y$10$MBypw7V7F5vtthuxm61eJO5gN72adM9PhQgypDnQbEZdPoHwFDlXW', 0, 0),
(2, 'Cool', 'Capibara', 'moderator@gmail.com', '/assets/profile/1688552191_f0cc871ed343516d.jpg', 'Downtown 1', '+373 123 123 123', '$2y$10$5YeOC3nB1Dd0g6rIAoE6BOny3W7nV6ixKcqA6G6sJ7UVyBbT7rsje', 1, 0);

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel Vote
--

DROP TABLE IF EXISTS Vote;
CREATE TABLE IF NOT EXISTS Vote (
  `id` int NOT NULL AUTO_INCREMENT,
  `isPositive` tinyint(1) DEFAULT NULL,
  `anonymousToken` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incidentId` int DEFAULT NULL,
  `userId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `incidentId` (`incidentId`),
  KEY `userId` (`userId`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Eliminarea datelor din tabel Vote
--

INSERT INTO Vote (`id`, `isPositive`, `anonymousToken`, `incidentId`, `userId`) VALUES
(1, 1, '1688550990_c74173771d504960ca3b', 1, 1),
(2, 1, '1688552544_12fd1b92f82cc13ecbbe', 1, 0),
(3, 0, '1688550990_c74173771d504960ca3b', 2, 1),
(4, 1, '', 3, 2),
(5, 1, '1688550990_c74173771d504960ca3b', 3, 1),
(6, 1, '1688553250_4b695acb8ad7c9449817', 3, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
