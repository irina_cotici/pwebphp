<?php
require "database/photo.php";

function processPhotoAdding($connection, $incidentId) {
    $photoPath = "";
    $photo = processFile("photo");

    if ($photo) {
        $photoPath = saveImageToServer($photo, "assets/profile/");
    }

    if ($photoPath) {
        processLogAdding($connection, "Added a photo to an incident");
        $photo = addPhoto(
            $connection,
            $photoPath,
            $incidentId
        );
        
        return [
            "success" => true,
            "validationErrors" => [],
            "error" => null,
            "data" => $photo,
        ];
    }

    return [
        "success" => false,
        "validationErrors" => [],
        "error" => null,
        "data" => null,
    ];
}

function processGetAllIncidentPhotos($connection, $incidentId) {
    $photos = getAllIncidentPhotos($connection, $incidentId);

    if (is_array($photos)) {
        processLogAdding($connection, "Got all incident's photos");
        return [
            "success" => true,
            "error" => null,
            "validationErrors" => [],
            "data" => $photos,
        ];
    }


    return [
        "success" => false,
        "validationErrors" => [],
        "error" => "Error occured when getting photos",
        "data" => null,
    ];
}

function processIncidentPhotoDelete($connection, $photoId, $photoPath) {
    $result = removePhotoById($connection, $photoId);

    if ($result) {
        removeImageFromServer($photoPath);

        return [
            "success" => true,
            "error" => null,
            "validationErrors" => [],
            "data" => true,
        ];
    }

    return [
        "success" => false,
        "error" => null,
        "validationErrors" => [],
        "data" => null,
    ];
}
?>
