<?php
require "database/log.php";

function processLogAdding(
    $connection,
    $message
) {
    $userId = isset($_SESSION['user']) ? $_SESSION['user']['id'] : '';
    $timestamp = date("Y-m-d H:i:s");
    
    $comment = addLog(
        $connection,
        $message,
        $timestamp,
        $userId,
    );
    
    if ($comment) {
        return [
            "success" => true,
            "validationErrors" => [],
            "error" => null,
            "data" => $comment,
        ];
    }

    return [
        "success" => false,
        "validationErrors" => [],
        "error" => "You already voted",
        "data" => null,
    ];
}

function processGetAllLogs($connection) {
    $logs = getAllLogs($connection);

    if (is_array($logs)) {
        return [
            "success" => true,
            "error" => null,
            "validationErrors" => [],
            "data" => $logs,
        ];
    }


    return [
        "success" => false,
        "validationErrors" => [],
        "error" => "Error occured when getting votes",
        "data" => null,
    ];
}

?>
