<?php
require "database/comment.php";

function processCommentAdding(
    $connection,
    $incidentId,
    $userId
) {
    $validationErrors = [];

    $description = processInput("description");
    $timestamp = date("Y-m-d H:i:s");

    $validationErrors = validateInput("description", $description, ["required" => true], $validationErrors);
    
    if (count($validationErrors) == 0) {
        processLogAdding($connection, "Added a comment");
        $comment = addComment(
            $connection,
            $description,
            $timestamp,
            $incidentId,
            $userId,
          );
        
        return [
            "success" => true,
            "validationErrors" => $validationErrors,
            "error" => null,
            "data" => $comment,
        ];
    }

    return [
        "success" => false,
        "validationErrors" => $validationErrors,
        "error" => "You already voted",
        "data" => null,
    ];
}

function processGetAllIncidentComments($connection, $incidentId) {
    $votes = getAllIncidentComments($connection, $incidentId);

    if (is_array($votes)) {
        processLogAdding($connection, "Got all comments");
        return [
            "success" => true,
            "error" => null,
            "validationErrors" => [],
            "data" => $votes,
        ];
    }


    return [
        "success" => false,
        "validationErrors" => [],
        "error" => "Error occured when getting votes",
        "data" => null,
    ];
}

?>
