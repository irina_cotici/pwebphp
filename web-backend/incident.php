<?php
require 'database/incident.php';

function processIncidentCreation($connection, $userId) {
    $title = processInput("title");
    $description = processInput("description");
    $address = processInput("address");
    $keywords = processInput("keywords");

    
    $incident = addIncident(
        $connection,
        $title,
        $description,
        $address,
        $keywords,
        $userId,
    );
        
    if ($incident) {
        processLogAdding($connection, "Added a incident");
        return [
            "success" => true,
            "validationErrors" => [],
            "error" => null,
            "data" => $incident,
        ];
    }

    return [
        "success" => false,
        "validationErrors" => [],
        "error" => null,
        "data" => null,
    ];
}

function processGetAllIncidents($connection) {
    $incidents = getAllIncidents($connection);

    if (is_array($incidents)) {
        processLogAdding($connection, "Got all incidents");
        return [
            "success" => true,
            "error" => null,
            "validationErrors" => [],
            "data" => $incidents,
        ];
    }


    return [
        "success" => false,
        "validationErrors" => [],
        "error" => "Error occured when getting incidents",
        "data" => null,
    ];
}

function processGetUserIncidents($connection, $userId) {
    $incidents = getUserIncidents($connection, $userId);

    if (is_array($incidents)) {
        processLogAdding($connection, "Got his incidents");
        return [
            "success" => true,
            "error" => null,
            "validationErrors" => [],
            "data" => $incidents,
        ];
    }


    return [
        "success" => false,
        "validationErrors" => [],
        "error" => "Error occured when getting incidents",
        "data" => null,
    ];
}

function processIncidentDelete($connection, $incidentId) {
    $result = removeIncidentById($connection, $incidentId);

    if ($result) {
        processLogAdding($connection, "Deleted an incident");
        return [
            "success" => true,
            "error" => null,
            "validationErrors" => [],
            "data" => true,
        ];
    }

    return [
        "success" => false,
        "error" => null,
        "validationErrors" => [],
        "data" => null,
    ];
}

function processGetIncidentById($connection, $incidentId) {
    $incident = getIncidentById($connection, $incidentId);

    if ($incident) {
        processLogAdding($connection, "Got an incident");
        return [
            "success" => true,
            "error" => null,
            "validationErrors" => [],
            "data" => $incident,
        ];
    }


    return [
        "success" => false,
        "validationErrors" => [],
        "error" => "Error occured when getting incident",
        "data" => null,
    ];
}

function processIncidentUpdate($connection, $incidentId) {
    $validationErrors = [];

    $title = processInput("title");
    $description = processInput("description");
    $address = processInput("address");
    $keywords = processInput("keywords");
    $status = processInput("status");

    $validationErrors = validateInput("title", $title, ["required" => true], $validationErrors);
    $validationErrors = validateInput("description", $description, ["required" => true], $validationErrors);
    $validationErrors = validateInput("address", $address, ["required" => true], $validationErrors);
    $validationErrors = validateInput("keywords", $keywords, ["required" => true], $validationErrors);

    if (count($validationErrors) == 0) {
        $updateResult = updateIncident(
            $connection,
            $title,
            $description,
            $address,
            $keywords,
            $status,
            $incidentId,
        );

        if ($updateResult) {
            processLogAdding($connection, "Edited an incident");
            $incident = getIncidentById($connection, $incidentId);

            return [
                "success" => true,
                "validationErrors" => [],
                "error" => null,
                "data" => $incident,
            ];
        }
    }

    return [
        "success" => false,
        "validationErrors" => $validationErrors,
        "error" => null,
        "data" => null,
    ];
}
?>
