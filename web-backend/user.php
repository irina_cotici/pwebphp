<?php
function processUserLogin($connection) {
    $validationErrors = [];

    $email = processInput("email");
    $password = processInput("password");

    $validationErrors = validateInput("email", $email, ["required" => true, "email" => true], $validationErrors);
    $validationErrors = validateInput("password", $password, ["required" => true], $validationErrors);
    
    if (count($validationErrors) == 0) {
        $user = getUserByEmail($connection, $email);

        if ($user && password_verify($password, $user["password"])) {
            processLogAdding($connection, "Logged in in the system");

            return [
                "success" => true,
                "validationErrors" => [],
                "error" => null,
                "data" => $user,
            ];
        }
    }

    return [
        "success" => false,
        "validationErrors" => $validationErrors,
        "error" => null,
        "data" => null,
    ];
}

function processUserRegister($connection) {
    $validationErrors = [];

    $firstname = processInput("firstname");
    $lastname = processInput("lastname");
    $address = processInput("address");
    $phone = processInput("phone");
    $email = processInput("email");
    $password = processInput("password");
    $repeatPassword = processInput("repeatPassword");
    $role = processInput("role");
    $status = processInput("status");
    $photoPath = "";

    $validationErrors = validateInput("firstname", $firstname, ["required" => true], $validationErrors);
    $validationErrors = validateInput("lastname", $lastname, ["required" => true], $validationErrors);
    $validationErrors = validateInput("phone", $phone, ["required" => true, "phone" => true], $validationErrors);
    $validationErrors = validateInput("email", $email, ["required" => true, "email" => true], $validationErrors);
    $validationErrors = validateInput("password", $password, ["required" => true, "min" => 8], $validationErrors);
    $validationErrors = validateInput("repeatPassword", $repeatPassword, ["required" => true, "match" => ["fieldName" => "Password", "value" => $password]], $validationErrors);
    $validationErrors = validateInput("address", $address, ["required" => true], $validationErrors);
    $validationErrors = validateInput("role", $role, ["required" => true, "number" => true], $validationErrors);
    $validationErrors = validateInput("status", $status, ["required" => true, "number" => true], $validationErrors);

    $photo = processFile("photo");

    if ($photo) {
        $photoPath = saveImageToServer($photo, "assets/profile/");
    }

    if (count($validationErrors) == 0) {
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
        processLogAdding($connection, "Added a User");

        $user = addUser(
            $connection,
            $email,
            $hashedPassword,
            $firstname,
            $lastname,
            $photoPath,
            $address,
            $phone,
            $role,
            $status
        );
        
        return [
            "success" => true,
            "validationErrors" => [],
            "error" => null,
            "data" => $user,
        ];
    }

    return [
        "success" => false,
        "validationErrors" => $validationErrors,
        "error" => null,
        "data" => null,
    ];
}

function processGetAllUsers($connection) {
    $users = getAllUsers($connection);

    if (is_array($users)) {
        processLogAdding($connection, "Got all users");
        return [
            "success" => true,
            "error" => null,
            "validationErrors" => [],
            "data" => $users,
        ];
    }


    return [
        "success" => false,
        "validationErrors" => [],
        "error" => "Error occured when getting users",
        "data" => null,
    ];
}

function registerFirstAdmin($connection) {

    if (count(getAllUsers($connection)) != 0) {
        return [
            "success" => false,
            "error" => "Admin should be first user!",
            "validationErrors" => [],
            "data" => null,
        ];
    }

    $firstname = "Admin";
    $lastname = "Admin";
    $address = "";
    $phone = "";
    $email = "admin@gmail.com";
    $password = "admin";
    $role = 0;
    $status = 0;
    $photoPath = "";

    $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

    $user = addUser(
        $connection,
        $email,
        $hashedPassword,
        $firstname,
        $lastname,
        $photoPath,
        $address,
        $phone,
        $role,
        $status
    );

    if ($user) {
        return [
            "success" => true,
            "validationErrors" => [],
            "error" => null,
            "data" => $user,
        ];
    }

    return [
        "success" => false,
        "validationErrors" => [],
        "error" => "Error occured when creating admin",
        "data" => null,
    ];
}

function processUserDelete($connection, $userId) {
    $result = removeUserById($connection, $userId);

    if ($result) {
        processLogAdding($connection, "Deleted a user");
        return [
            "success" => true,
            "error" => null,
            "validationErrors" => [],
            "data" => true,
        ];
    }

    return [
        "success" => false,
        "error" => null,
        "validationErrors" => [],
        "data" => null,
    ];
}

function processGetUserById($connection, $userId) {
    $user = getUserById($connection, $userId);

    if ($user) {
        processLogAdding($connection, "Got a user");
        return [
            "success" => true,
            "error" => null,
            "validationErrors" => [],
            "data" => $user,
        ];
    }


    return [
        "success" => false,
        "validationErrors" => [],
        "error" => "Error occured when getting user",
        "data" => null,
    ];
}

function processUpdateUser($connection, &$user) {
    $validationErrors = [];

    $firstname = processInput("firstname");
    $lastname = processInput("lastname");
    $address = processInput("address");
    $phone = processInput("phone");
    $email = processInput("email");
    $password = processInput("password");
    $repeatPassword = processInput("repeatPassword");
    $role = processInput("role");
    $status = processInput("status");
    $photoPath = $user["photo"];

    $validationErrors = validateInput("firstname", $firstname, ["required" => true], $validationErrors);
    $validationErrors = validateInput("lastname", $lastname, ["required" => true], $validationErrors);
    $validationErrors = validateInput("phone", $phone, ["required" => true, "phone" => true], $validationErrors);
    $validationErrors = validateInput("email", $email, ["required" => true, "email" => true], $validationErrors);
    $validationErrors = validateInput("address", $address, ["required" => true], $validationErrors);
    $validationErrors = validateInput("role", $role, ["required" => true, "number" => true], $validationErrors);
    $validationErrors = validateInput("status", $status, ["required" => true, "number" => true], $validationErrors);

    $photo = processFile("photo");

    if ($photo) {
        $photoPath = saveImageToServer($photo, "assets/profile/");
    }

    $user["firstname"] = $firstname;
    $user["lastname"] = $lastname;
    $user["address"] = $address;
    $user["phone"] = $phone;
    $user["email"] = $email;
    $user["password"] = $password;
    $user["repeatPassword"] = $repeatPassword;
    $user["role"] = $role;
    $user["status"] = $status;
    $user["photo"] = $photoPath;

    if (count($validationErrors) == 0) {
        $success = updateUser(
            $connection,
            $user["id"],
            $email,
            $firstname,
            $lastname,
            $photoPath,
            $address,
            $phone,
            $role,
            $status
        );
        
        if ($success) {
            processLogAdding($connection, "Updated a User");
            return [
                "success" => true,
                "validationErrors" => [],
                "error" => null,
                "data" => $user,
            ];
        }
    }

    return [
        "success" => false,
        "validationErrors" => $validationErrors,
        "error" => null,
        "data" => null,
    ];
}
?>
