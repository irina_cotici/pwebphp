<?php
function saveImageToServer($image, $uploadDirectory) {

    $filename = $image["name"];

    $fileTmpPath = $image["tmp_name"];

    $fileExtension = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
    $randomString = bin2hex(random_bytes(8));
    $newFileName = time() . '_' . $randomString . '.' . $fileExtension;

    
    $destination = $uploadDirectory . $newFileName;
    move_uploaded_file($fileTmpPath, $destination);
    return $uploadDirectory . $newFileName;
}

function removeImageFromServer($photoPath) {    
    $serverPath = $_SERVER['DOCUMENT_ROOT'] . $photoPath;
    
    if (file_exists($serverPath)) {
        if (unlink($serverPath)) {
            return true;
        } else {
            return false;
        }
    }
    
    return false;
}
?>