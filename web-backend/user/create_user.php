<?php
require_once 'db_connect.php';
require_once 'validation_functions.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $username = $_POST['username'];
  $email = $_POST['email'];
  $password = $_POST['password'];

  if (validateUsername($username) && validateEmail($email) && validatePassword($password)) {
    $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

    $sql = "INSERT INTO users (username, email, password) VALUES (?, ?, ?)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("sss", $username, $email, $hashedPassword);
    
    if ($stmt->execute()) {
      echo "User created successfully.";
    } else {
      echo "Error creating user.";
    }
  } else {
    echo "Invalid form data.";
  }
}
?>
