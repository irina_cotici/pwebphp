<?php
require "database/vote.php";

function processVoteAdding(
    $connection,
    $isPositive,
    $anonymousToken,
    $incidentId,
    $userId
) {
    $checkVote = false;

    if ($anonymousToken) {
        $checkVote = getVoteByAnonymousToken($connection, $anonymousToken, $incidentId);
    }
    elseif ($userId) {
        $checkVote = getVoteByUserId($connection, $userId, $incidentId);
    }

    if ($checkVote && $anonymousToken) {
        editVoteByToken(
            $connection,
            $isPositive,
            $anonymousToken,
            $checkVote["id"]
        );
    }
    else if ($checkVote && $userId) {
        editVoteByUserId(
            $connection,
            $isPositive,
            $userId,
            $checkVote["id"]
        );
    }
    else {
        processLogAdding($connection, "Voted for an incident");
        $vote = addVote(
            $connection,
            $isPositive,
            $anonymousToken,
            $incidentId,
            $userId,
          );
        
        return [
            "success" => true,
            "validationErrors" => [],
            "error" => null,
            "data" => $vote,
        ];
    }

    return [
        "success" => false,
        "validationErrors" => [],
        "error" => "You already voted",
        "data" => null,
    ];
}

function processGetAllIncidentVotes($connection, $incidentId) {
    $votes = getAllIncidentVotes($connection, $incidentId);

    if (is_array($votes)) {
        processLogAdding($connection, "Got all incident's votes");
        return [
            "success" => true,
            "error" => null,
            "validationErrors" => [],
            "data" => $votes,
        ];
    }


    return [
        "success" => false,
        "validationErrors" => [],
        "error" => "Error occured when getting votes",
        "data" => null,
    ];
}

?>
