<?php
require 'database/db_connection.php';
require 'database/user.php';
require 'database/manage_database.php';
require 'web-backend/image.php';
require 'web-backend/log.php';
require 'web-backend/user.php';
require 'web-frontend/header.php';
require 'web-frontend/footer.php';
require 'functions.php';
require_once 'constants.php';

session_start();
$isAuth = isset($_SESSION['authenticated']) && $_SESSION['authenticated'] === true;
$isAdmin = isset($_SESSION['authenticated']) && $_SESSION['user']['role'] == 0;

?>

<!DOCTYPE html>
<html>
	<head>
		<title>Incidents website</title>
		<link rel="stylesheet" href="assets/style.css">
	</head>
	<body>
		<?php
		renderHeader($isAuth, $isAdmin);
    $page = $_GET['page'] ?? 'home';
    switch ($page) {
			case 'view-incidents':
				include 'web-frontend/pages/view-incidents.php';
				break;
			case 'add-user':
				include 'web-frontend/register.php';
				break;
			case 'view-incident':
				include 'web-frontend/pages/view-incident.php';
				break;	
			case 'edit-incident':
				include 'web-frontend/pages/edit-incident.php';
				break;
			case 'create-incident':
				include 'web-frontend/pages/create-incident.php';
				break;
			case 'my-incidents':
				include 'web-frontend/pages/my-incidents.php';
				break;
			case 'manage-users':
				include 'web-frontend/pages/manage-users.php';
				break;
			case 'view-logs':
				include 'web-frontend/pages/view-logs.php';
				break;
			case 'manage-database':
				include 'web-frontend/pages/manage-database.php';
				break;
			case 'user-edit':
				include 'web-frontend/user-edit.php';
				break;
			default:
				include 'web-frontend/pages/home.php';
		}
		renderFooter();
		?>
	</body>
</html>
